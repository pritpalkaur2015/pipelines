﻿namespace Pipelines
{
    public interface IColorService
    {
        string Red();

        string Green();

        string Blue();
    }

    public class ColorService : IColorService
    {
        public string Red()
        {
            return "Red";
        }

        public string Green()
        {
            return "Green";
        }

        public string Blue()
        {
            return "Blue";
        }
    }
}
